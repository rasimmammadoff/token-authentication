
from django.contrib import admin
from django.urls import path
from core.views import HelloView,UserCreate
from rest_framework.authtoken import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('register/', UserCreate.as_view(), name='register'),
    path('login/', views.obtain_auth_token, name='api-token-auth'),
    path('hello/',index.as_view(),name='hello'),
]
