from django.contrib.auth.models import User
from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):
    password2 = serializers.CharField(max_length=20,write_only=True)
    class Meta:
        model = User
        fields = ('username','email','password','password2')

    def create(self,validated_data):
        password = validated_data.pop('password')
        password2 = validated_data.pop('password2')
        if(password == password2):
            user = User(**validated_data)
            user.set_password(password)
            user.save()
            return user
        else:
            raise serializers.ValidationError('Passwords do not match!')