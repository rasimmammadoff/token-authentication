from django.contrib.auth import authenticate, login
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated 
from  .serializer import UserSerializer


class index(APIView):
    
    permission_classes=(IsAuthenticated,)
    
    def get(self,request):
        content = {'Message':'Hello World'}
        return Response(content)

class UserCreate(APIView):
    def post(self,request):
        data = JSONParser().parse(request)
        new_user = UserSerializer(data=data)
        if new_user.is_valid():
            new_user.save()
            return Response(new_user.data,status=status.HTTP_201_CREATED)
        else:
            return Response(new_user._errors,status=status.HTTP_400_BAD_REQUEST)





